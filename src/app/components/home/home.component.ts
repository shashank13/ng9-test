import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { MAT_HAMMER_OPTIONS } from '@angular/material/core';
import { trimTrailingNulls } from '@angular/compiler/src/render3/view/util';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public x = 0;
  progressInterval;
  constructor() { }

  ngOnInit(): void {

  }

  trigger = () =>{
    // console.log(this.x);
    // let timer = setInterval(trim, 2000);
    // function trim(){
    //   console.log(this.x);
    //   console.log("this.x++");
    //   this.x++;
    //   // if(this.x > 5) {
    //   //   clearInterval(timer);
    //   // }
    //   // console.log('post-interval');
    // }
    this.progressInterval = setInterval(() => {
      this.x += 10;
      console.log('this.counter', this.x);

      if(this.x >= 100){
        clearInterval(this.progressInterval);
        console.log('cleaned and finished');
      }
    },2000);
  }

}
