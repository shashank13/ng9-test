import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: any;
  formSubmitted: boolean;

  constructor(
    private fb: FormBuilder,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private snackBar: MatSnackBar,
    private router: Router

  ) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ["", [Validators.required]],
      password: ["", [Validators.required]]
    });

  }

  login = () =>{
    this.formSubmitted = true;
    console.log(this.loginForm.value);
    // this.userService.login(this.loginForm.value).subscribe({
    //   next: resp => {
    //     this.userService.me().subscribe({
    //       next: resp => {
    //         localStorage.setItem('userdata', JSON.stringify(resp.username));
    //       }
    //     });
    this.router.navigate(['/home']);
        // this.formSubmitted = false;
      // },
    //   error: (httpErrorResponse: HttpErrorResponse) => {
    //     this.formSubmitted = false;
    //     this.snackBar.open('Authentication Error', "OK", {
    //       duration: 300
    //     });
    //   }
    // });

  }

}
