import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { filter, map, mergeMap } from 'rxjs/operators';
// import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  caxLogo;
  path: string;
  userData: any;
  user_display_name: any;

  constructor(
    // private userService: UserService,
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private snackBar: MatSnackBar,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
  ) {
    // this.iconRegistry.addSvgIcon(
    //   'cax',
    //   this.sanitizer.bypassSecurityTrustResourceUrl(
    //     'assets/images/cax-logo.svg'
    //   )
    // );
    // this.caxLogo = 'cax';
    // this.userService.me().subscribe({
    //   next: resp => {
    //     console.log(resp);
    //     this.userData = resp;
    //     this.user_display_name = this.userData.first_name +' '+this.userData.last_name;
    //     }
    // });
   }

  ngOnInit(): void {
    this.path = window.location.pathname;
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map(route => {
          while (route.firstChild) route = route.firstChild;
          return route;
        }),
        filter(route => route.outlet === 'primary'),
        mergeMap(route => route.data)
      )
      .subscribe(event => this.titleService.setTitle(event['title']));
  }
  logout = () => {
    // this.userService.logOut().subscribe({
    //   next: resp => {
    //     this.user_display_name = '';
    //     localStorage.clear();
    //     this.router.navigate(['']);
    //     this.snackBar.open('Logged Out Successfully !!', '', {
    //       duration: 2000,
    //     });
    //   },
    //   error: (HttpResponse: HttpErrorResponse) => {
    //     this.snackBar.open(`${HttpResponse.error.data}`, "OK", {
    //       duration: 3000
    //     });
    //   }
    // });
  }

}
