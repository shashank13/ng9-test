import { Injectable } from '@angular/core';
import { ENDPOINTS } from './endpoints';
// import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class Globals {
    private endpoints: any = ENDPOINTS;
    // private baseUrl: string = environment.baseUrl;
    constructor() { }

    /**
     * Basic URL Join
     * Joins Endpoints BaseURL with endpoint
     */
    urlJoin = (category: string, endpoint: string): string => {
        const generatedEndpoint = `${
            this.endpoints[category][endpoint]
            }`;
        return generatedEndpoint;
    }

    urlJoinWithParam = (category: string, endpoint: string, param: string) => {
        const generatedEndpoint = `${
            this.endpoints[category][endpoint].pathA
            }${param}${this.endpoints[category][endpoint].pathB}`;
        return generatedEndpoint;
    }
}
