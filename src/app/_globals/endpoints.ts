const ENDPOINTS: any = {
  users: {
    login: '/api/users/login',
    logout: '/api/users/logout',
    me: '/api/users/me'
  },
  home: {
    predict: '/api/predict/',
    get_attributes: '/api/get_attributes/',
    history: '/api/users/history',
    categories: '/api/taxonomy/categories/',
    sub_categories: '/api/taxonomy/sub_categories/',
    product_types: '/api/taxonomy/product_types/',
    example: '/api/users/example'
  }
};

export { ENDPOINTS };
